/**
 * 
 */
package gui;

import generation.Order;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * This class is a wrapper class to startup the Maze game as a Java application
 * 
 * This code is refactored code from Maze.java by Paul Falstad, www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 * 
 * TODO: use logger for output instead of Sys.out
 */
public class MazeApplication extends JFrame {

	// not used, just to make the compiler, static code checker happy
	private static final long serialVersionUID = 1L;
	private Controller controller;

	/**
	 * Constructor
	 */
	public MazeApplication() {
		init(null);
	}

	/**
	 * Constructor that loads a maze from a given file or uses a particular method to generate a maze
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that stores an already generated maze that is then loaded, or can be null
	 */
	public MazeApplication(String parameter) {
		init(parameter);
	}

	/**
	 * Instantiates a controller with settings according to the given parameter.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
	 * or a filename that contains a generated maze that is then loaded,
	 * or can be null
	 * @return the newly instantiated and configured controller
	 */
	 Controller createController(String parameter) {
	    // need to instantiate a controller to return as a result in any case
	    Controller result = new Controller(this) ;
	    String msg = null; // message for feedback
	    // Case 1: no input
	    if (parameter == null) {
	        msg = "MazeApplication: maze will be generated with a randomized algorithm."; 
	    }
	    // Case 2: Prim
	    else if ("Prim".equalsIgnoreCase(parameter))
	    {
	        msg = "MazeApplication: generating random maze with Prim's algorithm.";
	        result.setBuilder(Order.Builder.Prim);
	    }
	    // Case 3 a and b: Eller, Kruskal or some other generation algorithm
	    else if ("Kruskal".equalsIgnoreCase(parameter))
	    {
	    	// TODO: for P2 assignment, please add code to set the builder accordingly
	        throw new RuntimeException("Don't know anybody named Kruskal ...");
	    }
	    else if ("Eller".equalsIgnoreCase(parameter))
	    {
	    	msg = "MazeApplication: generating random maze with Eller's algorithm.";
	        result.setBuilder(Order.Builder.Eller);
	    	// TODO: for P2 assignment, please add code to set the builder accordingly
	        //throw new RuntimeException("Don't know anybody named Eller ...");
	    }
	    // Case 4: a file
	    else {
	        File f = new File(parameter) ;
	        if (f.exists() && f.canRead())
	        {
	            msg = "MazeApplication: loading maze from file: " + parameter;
	            result.setFileName(parameter);
	            return result;
	        }
	        else {
	            // None of the predefined strings and not a filename either: 
	            msg = "MazeApplication: unknown parameter value: " + parameter + " ignored, operating in default mode.";
	        }
	    }
	    // controller instanted and attributes set according to given input parameter
	    // output message and return controller
	    System.out.println(msg);
	    return result;
	}

	/**
	 * Initializes some internals and puts the game on display.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that contains a generated maze that is then loaded, or can be null
	 */
	private void init(String parameter) {
	    // instantiate a game controller and add it to the JFrame
		JPanel jpStartScreen = new JPanel();
	    JPanel jpPlayingScreen = new JPanel();
	    
		this.controller = createController(parameter);
		add(controller.getPanel()) ;
		
		String [] robotDriver = {"Wizard", "WallFollower", "Manual" };
		JComboBox<String> selectRobotDriver = new JComboBox<String>(robotDriver);
		ActionListener actionListenerDriver = new ActionListenerDriver(selectRobotDriver, controller);
		selectRobotDriver.addActionListener(actionListenerDriver);
		jpStartScreen.add(selectRobotDriver);
		
		String [] mazeGeneration = {"DFS", "Prim", "Eller"};
		JComboBox<String> selectMazeGeneration = new JComboBox<String>(mazeGeneration);
		ActionListener actionListenerMazeGeneration = new ActionListenerMazeGeneration(selectMazeGeneration, controller);
		selectMazeGeneration.addActionListener(actionListenerMazeGeneration);
		jpStartScreen.add(selectMazeGeneration);
		
		String [] skillLevel = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
		JComboBox<String> selectSkillLevel = new JComboBox<String>(skillLevel);
		ActionListener actionListenerSkillLevel = new ActionListenerSkillLevel(selectSkillLevel, controller);
		selectSkillLevel.addActionListener(actionListenerSkillLevel);
		jpStartScreen.add(selectSkillLevel);
		
		JButton startButton = new JButton("Start");
		ActionListener actionListenerStart = new ActionListenerStart(startButton, controller);
		startButton.addActionListener(actionListenerStart);
		jpStartScreen.add(startButton);
		
//		String [] failSensorsNames = {"Fail Left", "Fail Right", "Fail Forward", "Fail Backward"};
//		JComboBox<String> failSensor = new JComboBox<String>(failSensorsNames);
//		jpPlayingScreen.add(failSensor);
//		String [] repairSensorsNames = {"Repair Left", "Repair Right", "Repair Forward", "Repair Backward"};
//		JComboBox<String> repairSensor = new JComboBox<String>(repairSensorsNames);
//		jpPlayingScreen.add(repairSensor);
		
		controller.setPanel(jpStartScreen, jpPlayingScreen);
		//controller.setFailSensorComboBox(failSensor);
		//controller.setRepairSensorComboBox(repairSensor);
		add(jpStartScreen, BorderLayout.PAGE_START);
		add(jpPlayingScreen, BorderLayout.PAGE_END);
		//controller.setFrame(this);
		// instantiate a key listener that feeds keyboard input into the controller
		// and add it to the JFrame
		KeyListener kl = new SimpleKeyListener(this, controller) ;
		addKeyListener(kl) ;
		controller.initialKeyListener(kl);
		// set the frame to a fixed size for its width and height and put it on display
		setSize(400, 400) ;
		setVisible(true) ;
		// focus should be on the JFrame of the MazeApplication and not on the maze panel
		// such that the SimpleKeyListener kl is used
		setFocusable(true) ;
		// start the game, hand over control to the game controller
		controller.start(this);
	}
	
	public Controller getController() {
		return controller;
	}
	
	/**
	 * Main method to launch Maze game as a java application.
	 * The application can be operated in three ways. 
	 * 1) The intended normal operation is to provide no parameters
	 * and the maze will be generated by a randomized DFS algorithm (default). 
	 * 2) If a filename is given that contains a maze stored in xml format. 
	 * The maze will be loaded from that file. 
	 * This option is useful during development to test with a particular maze.
	 * 3) A predefined constant string is given to select a maze
	 * generation algorithm, currently supported is "Prim".
	 * @param args is optional, first string can be a fixed constant like Prim or
	 * the name of a file that stores a maze in XML format
	 */
	public static void main(String[] args) {
	    JFrame app ; 
		switch (args.length) {
		case 1 : app = new MazeApplication(args[0]);
		break ;
		case 0 : 
		default : app = new MazeApplication() ;
		break ;
		}
		app.repaint() ;
	}

}
