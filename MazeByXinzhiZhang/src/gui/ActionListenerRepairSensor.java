package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

/**
 * Create a class to implement ActionListener so that the repair can be triggered through the comboBox.
 * Help to repair the specified sensors.
 * @author Lulu Zhang
 *
 */
public class ActionListenerRepairSensor implements ActionListener {
	Controller controller;
	Robot robot;
	JComboBox<String> comboBox;
	
	public ActionListenerRepairSensor(JComboBox<String> comboBox, Controller controller) {
		this.controller = controller;
		this.robot = this.controller.getRobot();
		this.comboBox = comboBox;
	}
	/** 
	 * Listens to the combo box. 
	 * @param ActionEvent an action event given by user
	 * */
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("repair combobox is used");
		String repairedSensor = (String)comboBox.getSelectedItem();
		System.out.println(repairedSensor);
		switch(repairedSensor) {
		case "Repair Left":
			//System.out.println("action listener repair left");
			this.controller.setSensorThread("left sensor");
			break;
			//this.robot.repairFailedSensor(Robot.Direction.LEFT);
		case "Repair Right":
			this.controller.setSensorThread("right sensor");
			break;
			//this.robot.repairFailedSensor(Robot.Direction.RIGHT);
		case "Repair Forward":
			this.controller.setSensorThread("forward sensor");
			break;
			//this.robot.repairFailedSensor(Robot.Direction.FORWARD);
		case "Repair Backward":
			this.controller.setSensorThread("backward sensor");
			break;
			//this.robot.repairFailedSensor(Robot.Direction.FORWARD);
		}
	}
}

