package gui;

import gui.Robot.Direction;

/**
 * Create a class to implement the Runnable so that
 * each sensor can have a separate thread to operate the trigger/repair process.
 * @author Lulu Zhang
 *
 */
public class RobotSensor implements Runnable {
	Robot robot;
	Direction direction;
	RobotDriver driver;
	
	public RobotSensor(Robot robot, RobotDriver driver, Direction direction) {
		this.robot = robot;
		this.direction = direction;
		this.driver = driver;
	}
	
	public void run() {
		while (true) {
			if (robot.hasOperationalSensor(direction)) {
				robot.triggerSensorFailure(direction);
			}
			else {
				robot.repairFailedSensor(direction);
			}
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				robot.repairFailedSensor(direction);
				return;
			}
		}
	}
}
