package gui;

import generation.CardinalDirection;
import generation.Distance;
import generation.Floorplan;
import generation.Maze;
import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * This class implements the RobotDriver interface.
 * It includes the implementation of Wizard algorithm which uses the distances object
 * to guide the robot out of the maze.
 * This algorithm provides the most efficient methods and generates the shortest path length.
 * Responsibility: Guide the robot out of the maze by using the distance object.
 * Collaborator: Distance.java, robot.java
 * @author Lulu Zhang
 *
 */
public class Wizard implements RobotDriver {
	private Robot robot;
	private Distance distance;
	private boolean leftSensor;
	private boolean rightSensor;
	private boolean forwardSensor;
	private boolean backwardSensor;
	private Maze maze;
	private int width;
	private int height;
	private Controller controller;
	
	public Wizard(){
		this.leftSensor = true;
		this.rightSensor = true;
		this.forwardSensor = true;
		this.backwardSensor = true;
		this.robot = null;
		this.distance = null;
		this.width = 0; 
		this.height = 0;
	}
	public Wizard(Robot robot, Distance distance, int width, int height) {
		this.setRobot(robot);
		this.setDistance(distance);
		this.setDimensions(width, height);
		this.leftSensor = true;
		this.rightSensor = true;
		this.forwardSensor = true;
		this.backwardSensor = true;
	}
	/**
	 * Assigns a robot platform to the driver. 
	 * The driver uses a robot to perform, this method provides it with this necessary information.
	 * @param r robot to operate
	 */
	@Override
	public void setRobot(Robot r) {
		this.robot = r;
	}
	/**
	 * Provides the robot driver with information on the dimensions of the 2D maze
	 * measured in the number of cells in each direction.
	 * @param width of the maze
	 * @param height of the maze
	 * @precondition 0 <= width, 0 <= height of the maze.
	 */
	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
	}
	/**
	 * Provides the robot driver with information on the distance to the exit.
	 * Only some drivers such as the wizard rely on this information to find the exit.
	 * @param distance gives the length of path from current position to the exit.
	 * @precondition null != distance, a full functional distance object for the current maze.
	 */
	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}
	/**
	 * Update the all the four sensor flags so that driver can be aware of
	 * current state of each sensor
	 */
	@Override
	public void triggerUpdateSensorInformation() {
		this.leftSensor = this.robot.hasOperationalSensor(Direction.LEFT);
		this.rightSensor = this.robot.hasOperationalSensor(Direction.RIGHT);
		this.forwardSensor = this.robot.hasOperationalSensor(Direction.FORWARD);
		this.backwardSensor = this.robot.hasOperationalSensor(Direction.BACKWARD);
	}
	/**
	 * RobotDrive class implements Runnable to achieve multi-threading
	 * drive2Exit method is actually a run method in the robot
	 */
	@Override
	public void run() {
		try {
			drive2Exit();
		} catch(Exception e) {
			//
		}
	}
	/**
	 * Drives the robot towards the exit given it exists and 
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws exception if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		//Exceptions will be thrown if the the robot lacks of energy or it has stopped after crashing into a wall
		if (this.robot.getBatteryLevel() <= 0) {
			throw new Exception("Robot lacks of energy.");
		}
		else if (this.robot.hasStopped()) {
			throw new Exception("Robot hits a wall.");
		}
		int x = this.robot.getCurrentPosition()[0];
		int y = this.robot.getCurrentPosition()[1];
		CardinalDirection currentCD;
		int [] nextPos;
		int [] currentPos;
		Turn turn;
		//The exit position has a distance of 1
		while (this.distance.getDistanceValue(x, y) > 1) {
			//Update driver's information of sensor states 
			this.triggerUpdateSensorInformation();
			//If all sensors are failed, return false and return to the losing screen
			if (this.leftSensor == false && this.rightSensor == false && this.forwardSensor == false && this.backwardSensor == false) {
				this.robot.setBatteryLevel(0);
				((BasicRobot) this.robot).switchFromPlayingToLosingIfNecessary(x, y);
				return false;
			}
			currentPos = this.robot.getCurrentPosition();
			currentCD = this.robot.getCurrentDirection();
			//find the next position based on the current position and current direction
			nextPos = this.getNeighborCloserToExit(x, y, currentCD);
			//find the direction to turn based on current position and next position
			turn = this.getTurningDirection(currentCD, currentPos, nextPos);
			//if next position is in robot's current forward direction, turn will be null
			//if not null, rotate robot according to the turning direction 
			if (turn != null) {
				this.robot.rotate(turn);
				if (robot.getBatteryLevel() <= 0) {
					return false;
				}
			}
			//Check if the robot does not has sensor in forward direction
			//if it has forward sensor, use it to detect whether there is a wall in front of it
			//to determine jump or move
			if (forwardSensor) {
				try {
				if (this.robot.distanceToObstacle(Robot.Direction.FORWARD) == 0) {
					robot.jump();
					if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
						return false;
					}
				}
				else {
					robot.move(1, false);
					if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
						return false;
					}
				}
				} catch(UnsupportedOperationException e) {
					continue;
				}
			}
			else if  (forwardSensor == false && leftSensor == true){
//				//If forward Sensor is failed, use left sensor to check whether to jump
				try {
					if (this.leftSensor) {
						robot.rotate(Turn.RIGHT);
						if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
							return false;
						}
						if (this.robot.distanceToObstacle(Robot.Direction.LEFT) == 0) {
							robot.rotate(Turn.LEFT);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
							robot.jump();
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
						}
						else {
							robot.rotate(Turn.LEFT);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
							robot.move(1, false);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
						}
					}
				}
				catch (UnsupportedOperationException ex) {
					continue;
				}
			}
			else if (forwardSensor == false && leftSensor == false){
				//If both forward and left sensor is failed, use right sensor to check whether to jump
				if (this.rightSensor) {
//						System.out.println("right sensor is used"); 
//						System.out.println("initial direction");
//						System.out.println(this.robot.getCurrentDirection());	
					try {
						robot.rotate(Turn.LEFT);
						if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
							return false;
						}
						if (this.robot.distanceToObstacle(Robot.Direction.RIGHT) == 0) {
							robot.rotate(Turn.RIGHT);
//								System.out.println("afterwards direction");
//								System.out.println(this.robot.getCurrentDirection());
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
//							try {
//									System.out.println("before jump");
//									System.out.println(this.robot.getCurrentPosition()[0]);
//									System.out.println(this.robot.getCurrentPosition()[1]);
							robot.jump();
//									System.out.println("after jump");
//									System.out.println(this.robot.getCurrentPosition()[0]);
//									System.out.println(this.robot.getCurrentPosition()[1]);
//							}
//							catch (Exception exc) {
//								exc.printStackTrace();
//							}
								//robot.jump();
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
									return false;
							}
						}
						else {
							robot.rotate(Turn.RIGHT);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
							robot.move(1, false);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
							}
						}
					catch(UnsupportedOperationException ex) {
						continue;
					}
				}
				else if (this.backwardSensor) {
					
					try {
						//If forward, left and right sensors are failed, use backward sensor to check whether to jump
							robot.rotate(Turn.AROUND);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}
							if (this.robot.distanceToObstacle(Robot.Direction.BACKWARD) == 0) {
								robot.rotate(Turn.AROUND);
								if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
									return false;
								}
								robot.jump();
								if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
									return false;
								}
							}
							else {
								robot.rotate(Turn.AROUND);
								if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
									return false;
								}
								robot.move(1, false);
								if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
									return false;
								}
							}
						}
					catch (UnsupportedOperationException ex) {
						continue;
					}
				}
			}
			//update the value of x and y to the current position
			x = this.robot.getCurrentPosition()[0];
			y = this.robot.getCurrentPosition()[1];
		}
		//after reaching the exit, rotate robot accordingly to make it face towards the exit
		if (this.robot.isAtExit()) {//
			for (Robot.Direction direction : Robot.Direction.values()) {
				//try {
					if (this.robot.canSeeThroughTheExitIntoEternity(direction)) {
						turn = this.getTurningDirectionExit(direction);
						if (turn != null) {
							robot.rotate(turn);
							if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
								return false;
							}	
						}
					}
//				} catch (UnsupportedOperationException e) {
//					this.goToExitWhenSensorsFailed(direction);
//					if (robot.getBatteryLevel() <= 0 || robot.hasStopped() == true) {
//						return false;
//					}	
//				}
			}
		}
		return true;
	}
	
	/**
	 * A helper function used in drive2Exit to find a neighbor cell that is closer to exit
	 * When there is a wall between current cell and target cell, check whether worth it to jump over the wall
	 * @param x coordinate
	 * @param y coordinate
	 * @param currentCD
	 * @return An integer array which represents the coordinate of position closer to exit
	 */
	private int[] getNeighborCloserToExit(int x, int y, CardinalDirection currentCD) {
		// corner case, (x,y) is exit position
		if (this.distance.isExitPosition(x, y))
			return null;
		// find best candidate
		int dnext = this.distance.getDistanceValue(x, y) ;
		int[] result = new int[2] ;
		int[] dir;
		this.controller = ((BasicRobot) this.robot).getController();
		this.maze = this.controller.getMazeConfiguration();
//		System.out.println("maze width");
//		System.out.println(this.maze.getWidth());
//		System.out.println("maze height");
//		System.out.println(this.maze.getHeight());
//		System.out.println("initial position");
//		System.out.println(x);
//		System.out.println(y);
		for (CardinalDirection cd: CardinalDirection.values()) {
			dir = cd.getDirection();
			if (x+dir[0] >= 0 && x+dir[0] < this.width && y+dir[1] >= 0 && y+dir[1] < this.height) {
//				System.out.println("new position is valid");
//				System.out.println(dir[0]);
//				System.out.println(dir[1]);
//				System.out.println(cd);
				int dn = this.distance.getDistanceValue(x+dir[0], y+dir[1]);
				if (dn < dnext) {
//					System.out.println("new position is closer");
//					System.out.println(dir[0]);
//					System.out.println(dir[1]);
//					System.out.println(cd);
					//Robot.Direction direction = this.getRobotRelativeDirection(currentCD, cd);
					// if the robot does not has sensor in the specified direction, return null
					//then in the driver2Exit() method, if the null is returned, the drive method will return false, the robot will stop
//					if (this.robot.hasOperationalSensor(direction) == false) {
//						return null;
//					}	
					if (this.maze.hasWall(x, y, cd)) {//this.robot.distanceToObstacle(direction) == 0)
						//System.out.println("haswall");
//						System.out.println("has wall");
//						System.out.println(cd);
						if (this.betterToJump(x, y, x+dir[0], y+dir[1])) {
							result[0] = x+dir[0] ;
							result[1] = y+dir[1] ;
//							System.out.println(result[0]);
//							System.out.println(result[1]);
							dnext = dn;
						}
					}
					else {
						//System.out.println("nothaswall");
						result[0] = x+dir[0] ;
						result[1] = y+dir[1] ;
						dnext = dn ;
					}	
				}
			}
		}
		// expectation: we found a neighbor that is closer
		assert(this.distance.getDistanceValue(x, y) > dnext) : 
			"cannot identify direction towards solution: stuck at: " + x + ", "+ y ;
		// since assert statements need not be executed, check it 
		// to avoid giving back wrong result
		return (this.distance.getDistanceValue(x, y) > dnext) ? result : null;
	}
	
	/**
	 * A helpful function used in Drive2Exit
	 * Get a turning direction so that robot can turn to look into eternity
	 * @param direction
	 * @return turning direction
	 */
	private Turn getTurningDirectionExit(Direction direction) {
		//set turn = null so that when the robot's forward direction is the eternity
		//turn will remain to be null
		Turn turn = null;
		if (direction == Robot.Direction.LEFT) {
			turn = Turn.LEFT;
		}
		else if (direction == Robot.Direction.RIGHT) {
			turn = Turn.RIGHT;
		}
		else if (direction == Robot.Direction.BACKWARD) {
			turn = Turn.AROUND;
		}
		return turn;
	}
	/**
	 * A helpful function used in findNeighborCloserToExit to determine 
	 * whether it worth to jump through a wall to reach a closer position
	 * @param x
	 * @param y
	 * @param newx
	 * @param newy
	 * @return true if it's better to jump; false otherwise
	 */
	private boolean betterToJump(int x, int y, int newx, int newy) {
		int curDistance = this.distance.getDistanceValue(x, y);
		int newDistance = this.distance.getDistanceValue(newx, newy);
		//Since moving 1 step costs 5, if the closer position is 10 steps closer to exit
		//it will save at least 5*10 = 50 energy
		//So, better to jump if the distance between current and next position >= 10
		if ((curDistance - newDistance) >= 10){
			return true;
		}
		return false;
	}

	/**
	 * A helpful function used in drive2Exit to determine robot's turning direction
	 * based on the current position and the next position(the closer neighbor)
	 * so that after rotating, the robot can move/jump to the target position
	 * @param current CardinalDirection, currentPos, nextPos
	 * @return turning direction
	 */
	private Turn getTurningDirection(CardinalDirection currentCD, int[] currentPos, int[] nextPos){
		Robot.Turn turn = null;
		if (currentCD == CardinalDirection.East) {
			turn = this.getTurningDirectionEast(currentCD, currentPos, nextPos);
		}
		else if (currentCD == CardinalDirection.West) {
			turn = this.getTurningDirectionWest(currentCD, currentPos, nextPos);
		}
		else if (currentCD == CardinalDirection.North) {
			turn = this.getTurningDirectionNorth(currentCD, currentPos, nextPos);
		}
		else if (currentCD == CardinalDirection.South) {
			turn = this.getTurningDirectionSouth(currentCD, currentPos, nextPos);
		}
		return turn;
	}
	
	/**
	 * Function called in getTurningDirection. Called when the robot is facing South(CardinalDirection)
	 * to determine which direction to turn
	 * @param currentCD
	 * @param currentPos
	 * @param nextPos
	 * @return correct turning direction for robot to reach a closer neighbor
	 */
	private Turn getTurningDirectionSouth(CardinalDirection currentCD, int[] currentPos, int[] nextPos) {
		// TODO Auto-generated method stub
		Robot.Turn turn = null;
		int dx = nextPos[0] - currentPos[0];
		int dy = nextPos[1] - currentPos[1];
		if (dx == 1 && dy == 0) {
			turn = Robot.Turn.RIGHT;
		}
		else if (dx == -1 && dy == 0) {
			turn = Robot.Turn.LEFT;
		}
		else if (dx == 0 && dy == -1) {
			turn = Robot.Turn.AROUND;
		}
		else if (dx == 0 && dy == 1) {
			turn = null;
		}
		return turn;
	}
	/**
	 * Function called in getTurningDirection. Called when the robot is facing North(CardinalDirection)
	 * to determine which direction to turn
	 * @param currentCD
	 * @param currentPos
	 * @param nextPos
	 * @return correct turning direction for robot to reach a closer neighbor
	 */
	private Turn getTurningDirectionNorth(CardinalDirection currentCD, int[] currentPos, int[] nextPos) {
		// TODO Auto-generated method stub
		Robot.Turn turn = null;
		int dx = nextPos[0] - currentPos[0];
		int dy = nextPos[1] - currentPos[1];
		if (dx == 1 && dy == 0) {
			turn = Robot.Turn.LEFT;
		}
		else if (dx == -1 && dy == 0) {
			turn = Robot.Turn.RIGHT;
		}
		else if (dx == 0 && dy == 1) {
			turn = Robot.Turn.AROUND;
		}
		else if (dx == 0 && dy == -1) {
			turn = null;
		}
		return turn;
	}
	/**
	 * Function called in getTurningDirection. Called when the robot is facing West(CardinalDirection)
	 * to determine which direction to turn
	 * @param currentCD
	 * @param currentPos
	 * @param nextPos
	 * @return correct turning direction for robot to reach a closer neighbor
	 */
	private Turn getTurningDirectionWest(CardinalDirection currentCD, int[] currentPos, int[] nextPos) {
		Robot.Turn turn = null;
		int dx = nextPos[0] - currentPos[0];
		int dy = nextPos[1] - currentPos[1];
		if (dx == 1 && dy == 0) {
			turn = Robot.Turn.AROUND;
		}
		else if (dx == 0 && dy == 1) {
			turn = Robot.Turn.RIGHT;
		}
		else if (dx == 0 && dy == -1) {
			turn = Robot.Turn.LEFT;
		}
		else if (dx == -1 && dy == 0) {
			turn = null;
		}
		return turn;
	}
	/**
	 * Function called in getTurningDirection. Called when the robot is facing East(CardinalDirection)
	 * to determine which direction to turn
	 * @param currentCD
	 * @param currentPos
	 * @param nextPos
	 * @return correct turning direction for robot to reach a closer neighbor
	 */
	private Turn getTurningDirectionEast(CardinalDirection currentCD, int[] currentPos, int[] nextPos) {
		Robot.Turn turn = null;
		int dx = nextPos[0] - currentPos[0];
		int dy = nextPos[1] - currentPos[1];
		if (dx == -1 && dy == 0) {
			turn = Robot.Turn.AROUND;
		}
		else if (dx == 0 && dy == 1) {
			turn = Robot.Turn.LEFT;
		}
		else if (dx == 0 && dy == -1) {
			turn = Robot.Turn.RIGHT;
		}
		else if (dx == 1 && dy == 0) {
			turn = null;
		}
		return turn;
	}
	/**
	 * /**
	 * Returns the total energy consumption of the journey, i.e.,
	 * the difference between the robot's initial energy level at
	 * the starting position and its energy level at the exit position. 
	 * This is used as a measure of efficiency for a robot driver.
	 * @return total energy consumption of the journey
	 */
	@Override
	public float getEnergyConsumption() {
		return 3000 - this.robot.getBatteryLevel();
	}
	/**
	 * Returns the total length of the journey in number of cells traversed. 
	 * Being at the initial position counts as 0. 
	 * This is used as a measure of efficiency for a robot driver.
	 * @return total length of the journey
	 */
	@Override
	public int getPathLength() {
		return this.robot.getOdometerReading();
	}

}
