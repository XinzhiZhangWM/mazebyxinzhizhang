package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * Create a class to implement ActionListener so that the driver comboBox can receive inputs.
 * Help to select desired driver algorithms.
 * @author Lulu Zhang
 *
 */
public class ActionListenerDriver implements ActionListener {
	
	Controller controller;
	private JComboBox comboBox;
	
	public ActionListenerDriver(JComboBox comboBox, Controller controller) {
		this.controller = controller;
		this.comboBox = comboBox;
	}

	/** 
	 * Listens to the combo box. 
	 * @param ActionEvent an action event given by user
	 * */
	public void actionPerformed(ActionEvent e) {
		String robotDriverName = (String)comboBox.getSelectedItem();
		switch(robotDriverName) {
		case "Wizard":
			this.controller.setRobotDriver("Wizard");
			break;
		case "WallFollower":
			this.controller.setRobotDriver("WallFollower");
			this.controller.setPerfect(true);
			break;
		case "Manual":
			this.controller.setRobotDriver("Manual");
			break;
		}
	}
}