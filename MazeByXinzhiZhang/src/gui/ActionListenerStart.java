package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

/**
 * Create a class to implement ActionListener so that the start button can receive inputs.
 * Help to start the game after choosing a starting configuration.
 * @author Lulu Zhang
 *
 */
public class ActionListenerStart implements ActionListener {
	
	private javax.swing.JButton JButton;
	private Controller controller;
	public ActionListenerStart(javax.swing.JButton startButton, Controller controller) {
		this.controller = controller;
		this.JButton = startButton;
	}
	
	/** 
	 * Listens to the combo box. 
	 * @param ActionEvent an action event given by user
	 * */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.JButton) {
			int skillLevel = this.controller.skillLevel;
			this.controller.switchFromTitleToGenerating(skillLevel);
		}

	}

}
