package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;

import generation.Order;

/**
 * Create a class to implement ActionListener so that the the maze generation comboBox can receive inputs.
 * Help to select desired a maze generation algorithm.
 * @author Lulu Zhang
 *
 */
public class ActionListenerMazeGeneration implements ActionListener {
	
	Controller controller;
	private JComboBox comboBox;
	
	public ActionListenerMazeGeneration(JComboBox comboBox, Controller controller) {
		this.controller = controller;
		this.comboBox = comboBox;
	}
	
	
	/** 
	 * Listens to the combo box. 
	 * @param ActionEvent an action event given by user
	 * */
	public void actionPerformed(ActionEvent e) {
		String mazeGenerationAlgo = (String)comboBox.getSelectedItem();
		switch(mazeGenerationAlgo) {
		case "DFS":
			this.controller.setBuilder(Order.Builder.DFS);
			break;
		case "Prim":
			this.controller.setBuilder(Order.Builder.Prim);
			break;
		case "Eller":
			this.controller.setBuilder(Order.Builder.Eller);
			break;
		}
	}
}
