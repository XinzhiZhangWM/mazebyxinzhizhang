package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;

import generation.Order;

/**
 * Create a class to implement ActionListener so that the skill level comboBox can receive inputs.
 * Help to select a skill level.
 * @author Lulu Zhang
 *
 */
public class ActionListenerSkillLevel implements ActionListener {
	
	Controller controller;
	private JComboBox comboBox;
	
	public ActionListenerSkillLevel(JComboBox comboBox, Controller controller) {
		this.controller = controller;
		this.comboBox = comboBox;
	}

	/** 
	 * Listens to the combo box. 
	 * @param ActionEvent an action event given by user
	 * */
	public void actionPerformed(ActionEvent e) {
		String skillLevel = (String)comboBox.getSelectedItem();
		switch(skillLevel) {
		case "0":
			this.controller.setSkillLevel(0);
			break;
		case "1":
			System.out.println("skill level1");
			this.controller.setSkillLevel(1);
			break;
		case "2":
			this.controller.setSkillLevel(2);
			break;
		case "3":
			this.controller.setSkillLevel(3);
			break;
		case "4":
			this.controller.setSkillLevel(4);
			break;
		case "5":
			this.controller.setSkillLevel(5);
			break;
		case "6":
			this.controller.setSkillLevel(6);
			break;
		case "7":
			this.controller.setSkillLevel(7);
			break;
		case "8":
			this.controller.setSkillLevel(8);
			break;
		case "9":
			this.controller.setSkillLevel(9);
			break;
		case "10":
			this.controller.setSkillLevel(10);
			break;
		case "11":
			this.controller.setSkillLevel(11);
			break;
		case "12":
			this.controller.setSkillLevel(12);
			break;
		case "13":
			this.controller.setSkillLevel(13);
			break;
		case "14":
			this.controller.setSkillLevel(14);
			break;
		case "15":
			this.controller.setSkillLevel(15);
			break;
		}
	}
}
