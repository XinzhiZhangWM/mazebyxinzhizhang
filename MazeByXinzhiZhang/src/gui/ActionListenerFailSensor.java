package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import gui.Robot.Direction;

/**
 * Create a class to implement ActionListener so that the failure can be triggered through the comboBox.
 * Help to make the specified sensors fail.
 * @author Lulu Zhang
 *
 */
public class ActionListenerFailSensor implements ActionListener {
	Controller controller;
	Robot robot;
	JComboBox<String> comboBox;
	
	public ActionListenerFailSensor(JComboBox<String> comboBox, Controller controller) {
		this.controller = controller;
		this.robot = this.controller.getRobot();
		this.comboBox = comboBox;
	}
	
	/** 
	 * Listens to the combo box. 
	 * @param ActionEvent an action event given by user
	 * */
	@Override
	public void actionPerformed(ActionEvent e) {
		String failedSensor = (String)comboBox.getSelectedItem();
		switch(failedSensor) {
		case "Fail Left":
			this.controller.setSensorThread("left sensor");
			break;
		case "Fail Right":
			this.controller.setSensorThread("right sensor");
			break;
		case "Fail Forward":
			this.controller.setSensorThread("forward sensor");
			break;
		case "Fail Backward":
			this.controller.setSensorThread("backward sensor");
			break;
			//this.robot.triggerSensorFailure(Robot.Direction.BACKWARD);
		}
	}
}
