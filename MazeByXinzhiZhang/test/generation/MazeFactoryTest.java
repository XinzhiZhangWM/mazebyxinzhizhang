package generation;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

/**
 * This test is a black box test. 
 * The test cases check whether MazeFactory can deliver correct mazes regardless of the algorithms used to generate maze.
 * Basic properties of both perfect and imperfect mazes are tested here
 * @author Vivian Zhu and Lulu Zhang
 *
 */

public class MazeFactoryTest {

	/* These tests should work for any maze, 
	 * regardless of the algorithm
	 * 
	 */
	private MazeFactory mazefactory1;
	private MazeFactory mazefactory2;
	private MazeFactory mazefactory3;
	
	
	private OrderStub order1;
	
	@Before
	public void setUp() {
		mazefactory1 = new MazeFactory();
		mazefactory2 = new MazeFactory(true);
		mazefactory3 = new MazeFactory(false);
		
		
		order1 = new OrderStub();
	} 
	
	@Test
	/**
	 *  see if the constructor is creating the object correctly
	 *  <p>
	 *  Method under test: MazeFactory own set up
	 *  <p>
	 *  It is correct if the mazefactory field is not null
	 */
	public final void testMazeFactory() {
		assertNotNull(mazefactory1);
		assertNotNull(mazefactory2);
		assertNotNull(mazefactory3);
	}
	
	@Test
	/**
	 *  see if the OrderStub is working
	 *  <p>
	 *  Method under test: OrderStub
	 *  <p>
	 *  It is correct if the OrderStub is not null
	 */
	public final void testOrderStub() {
		order1 = new OrderStub();
		assertNotNull(order1);
	}
	
	
	
	@Test
	/**
	 *  test to see if the distance matrix inside the maze configuration is infinity in non-perfect Prim
	 *  (infinity means this cell cannot reach the exit position
	 *  This test has been performed on all skillLevels
	 *  <p>
	 *  Method under test: MazeConfiguration, Distance, Distance.getAllDistanceValues
	 *  <p>
	 *  It is correct if every single cell can reach the exit
	 */
	public final void testMazeFactoryDistanceNotInfinityPrim() {
		order1 = new OrderStub();
		order1.setBuilder(Order.Builder.Prim);
		order1.setperfect(false);

		for (int k = 0; k < 10; k++) {
			order1.setSkillLevel(k);
			mazefactory1.order(order1);
			mazefactory1.waitTillDelivered();
			
			Distance distance = order1.getMazeConfig().getMazedists();
			int[][] distanceValue = distance.getAllDistanceValues();
			for (int i = 0; i < distanceValue.length; i++) {
				for (int j = 0; j < distanceValue[0].length; j++) {
					assertNotEquals(distanceValue[i][j], Integer.MAX_VALUE);
				}
			}
		}
	}
	
	@Test
	/**
	 *  Test case: test to see if the distance matrix inside the maze configuration is infinity in non-perfect Eller maze
	 *  (infinity means this cell cannot reach the exit position
	 *  This test has been performed on all skillLevels
	 *  <p>
	 *  Method under test: MazeConfiguration, Distance, Distance.getAllDistanceValues
	 *  <p>
	 *  It is correct if every single cell can reach the exit
	 */
	public final void testMazeFactoryDistanceNotInfinityNonPerfectEller() {
		order1 = new OrderStub();
		order1.setBuilder(Order.Builder.Eller);
		order1.setperfect(false);

		for (int k = 0; k < 10; k++) {
			order1.setSkillLevel(k);
			mazefactory1.order(order1);
			mazefactory1.waitTillDelivered();
			
			Distance distance = order1.getMazeConfig().getMazedists();
			int[][] distanceValue = distance.getAllDistanceValues();
			for (int i = 0; i < distanceValue.length; i++) {
				for (int j = 0; j < distanceValue[0].length; j++) {
					assertNotEquals(distanceValue[i][j], Integer.MAX_VALUE);
				}
			}
		}
	}
	
	@Test
	/**
	 * Test case: see if the distance matrix inside the maze configuration is infinity in perfect Eller maze
	 * <p>
	 * Method Under Test: MazeConfiguration, Distance, Distance.getAllDistanceValues
	 * <p>
	 * Correct Behavior: It is correct if every single cell can reach the exit
	 */
	public final void testMazeFactoryDistanceNotInfinityPerfectEller() {
		order1 = new OrderStub();
		order1.setBuilder(Order.Builder.Eller);
		order1.setperfect(true);
		
		for (int k = 0; k < 10; k++) {
			order1.setSkillLevel(k);
			mazefactory1.order(order1);
			mazefactory1.waitTillDelivered();
			
			Distance distance = order1.getMazeConfig().getMazedists();
			int[][] distanceValue = distance.getAllDistanceValues();
			for (int i = 0; i < distanceValue.length; i++) {
				for (int j = 0; j < distanceValue[0].length; j++) {
					assertNotEquals(distanceValue[i][j], Integer.MAX_VALUE);
				}
			}
		}
	}
	
	@Test
	/**
	 * Test case: see if perfect maze has one exit
	 * <p>
	 * Method Under Test: getAllDistanceValues()
	 * <p>
	 * Correct Behavior: maze only has one exit
	 */
	public final void testMazePerfectHasOneExit() {
		order1 = new OrderStub();
		order1.setBuilder(Order.Builder.Eller);
		order1.setperfect(true);
		for (int k = 0; k < 10; k++) {
			int count = 0;
			order1.setSkillLevel(1);
			mazefactory1.order(order1);
			mazefactory1.waitTillDelivered();
			
			Distance distance = order1.getMazeConfig().getMazedists();
			int[][] distanceValue = distance.getAllDistanceValues();
			
			for (int i = 0; i < distanceValue.length; i++) {
				for (int j = 0; j < distanceValue[0].length; j++) {
					if (distance.isExitPosition(i, j)) {
						count++;
					}
				}
			}
		assertEquals(count,1);
		}
	}
	@Test
	/**
	 * Test case: see if non-perfect maze has only one exit
	 * <p>
	 * Method Under Test: distance.getAllDistanceValues(), FloorPlan.setExitPosition()
	 * <p>
	 * Correct Behavior: non-perfect maze has only one exit
	 */
	public final void testMazeNotPerfectHasOneExit() {
		order1 = new OrderStub();
		order1.setBuilder(Order.Builder.Eller);
		order1.setSkillLevel(1);
		order1.setperfect(false);
		mazefactory1.order(order1);
		mazefactory1.waitTillDelivered();
		Distance distance = order1.getMazeConfig().getMazedists();
		int[][] distanceValue = distance.getAllDistanceValues();
		int count = 0;
		for (int i = 0; i < distanceValue.length; i++) {
			for (int j = 0; j < distanceValue[0].length; j++) {
				if (distance.isExitPosition(i, j)) {
					count++;
				}
			}
		}
		assertEquals(count,1);
	}
	@Test
	/**
	 * Test case: see if there is enough walls in this maze to make the whole game interesting to play
	 * <p>
	 * Method Under Test: 
	 * <p>
	 * Correct Behavior:
	 */
	public final void testMazePlayable() {
		order1 = new OrderStub();
		order1.setBuilder(Order.Builder.Eller);
		order1.setSkillLevel(1);
		mazefactory1.order(order1);
		mazefactory1.waitTillDelivered();
		//test to see if there is enough walls in this maze to make the whole game 
		//interesting to play
		
		Floorplan floorplan = order1.getMazeConfig().getFloorplan();
		Distance distance = order1.getMazeConfig().getMazedists();
		int[][] distanceValue = distance.getAllDistanceValues();
		int count = 0;
		for (int i = 0; i < distanceValue.length; i++) {
			for (int j = 0; j < distanceValue[0].length; j++) {
				Wallboard wallboard = new Wallboard(i,j,CardinalDirection.South);
				if (floorplan.hasWall(i, j, CardinalDirection.South) && !floorplan.isPartOfBorder(wallboard)) {
					count++;
				}
				
				Wallboard wallboard2 = new Wallboard(i,j,CardinalDirection.East);
				if (floorplan.hasWall(i, j, CardinalDirection.East) && !floorplan.isPartOfBorder(wallboard2)) {
					count++;
				}
			}
		}
		assertNotEquals(0, count);
	}
	
	
	@Test
	/**
	 * Test case: see when deterministic is true whether two perfect mazes equal to each other
	 * <p>
	 * Method Under Test: getMazeConfig() equals()
	 * <p>
	 * Correct Behavior: two mazes should be the same
	 */
	public final void testPerfectMazeEquals() {
		order1 = new OrderStub(5, Order.Builder.Eller, true);
		order1.setperfect(true);
		OrderStub order2 = new OrderStub(5, Order.Builder.Eller, true);
		order1.setperfect(true);
		MazeFactory mazefactory = new MazeFactory(true);
		MazeFactory mazefactory1 = new MazeFactory(true);
		
		mazefactory.order(order1);
		mazefactory.waitTillDelivered();
		mazefactory1.order(order2);
		mazefactory1.waitTillDelivered();
		assertTrue(order1.getMazeConfig().getFloorplan().equals(order2.getMazeConfig().getFloorplan()));
	}
	
	@Test
	/**
	 * Test case: see when deterministic is true whether two non-perfect mazes equal to each other
	 * <p>
	 * Method Under Test: getMazeConfig() equals()
	 * <p>
	 * Correct Behavior: two mazes should be the same
	 */
	public final void testNonPerfectMazeEquals() {
		order1 = new OrderStub(5, Order.Builder.Eller, true);
		order1.setperfect(false);
		OrderStub order2 = new OrderStub(5, Order.Builder.Eller, true);
		order2.setperfect(false);
		MazeFactory mazefactory = new MazeFactory(true);
		MazeFactory mazefactory1 = new MazeFactory(true);
		
		mazefactory.order(order1);
		mazefactory.waitTillDelivered();
		mazefactory1.order(order2);
		mazefactory1.waitTillDelivered();
		assertTrue(order1.getMazeConfig().getFloorplan().equals(order2.getMazeConfig().getFloorplan()));
	}
	
	@Test
	/**
	 * Test case: see if imperfect maze has rooms
	 * <p>
	 * Method Under Test: areaOverlapsWithRoom()
	 * <p>
	 * Correct Behavior: imperfect maze should have rooms
	 */
	public final void testImperfectMazeHasRoom() {
		order1 = new OrderStub(5, Order.Builder.Eller, false);
		MazeFactory mazefactory = new MazeFactory(true);
		mazefactory.order(order1);
		mazefactory.waitTillDelivered();
		Floorplan floorplan = order1.getMazeConfig().getFloorplan();
		assertTrue(floorplan.areaOverlapsWithRoom(1, 1, order1.getMazeConfig().getWidth()-2, order1.getMazeConfig().getHeight()-2));
	}
	
	@Test
	/**
	 * Test case: see if perfect maze has rooms
	 * <p>
	 * Method Under Test: areaOverlapsWithRoom()
	 * <p>
	 * Correct Behavior: perfect maze should have no room
	 */
	public final void testPerfectMazeHasNoROom() {
		order1 = new OrderStub(5, Order.Builder.Eller, true);
		MazeFactory mazefactory = new MazeFactory(true);
		mazefactory.order(order1);
		mazefactory.waitTillDelivered();
		Floorplan floorplan = order1.getMazeConfig().getFloorplan();
		assertFalse(floorplan.areaOverlapsWithRoom(1, 1, order1.getMazeConfig().getWidth()-2, order1.getMazeConfig().getHeight()-2));
		
	}
}
