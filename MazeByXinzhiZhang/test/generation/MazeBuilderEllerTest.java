package generation;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Test;

/**
 * The purpose of this junit test file is to test the Eller's algorithm for generating maze
 * It has tested all the functions that we have written for Eller's algorithm and made
 * sure it is getting the correct output. 
 * @author @author Vivian Zhu and Lulu Zhang
 *
 */

class MazeBuilderEllerTest {
	
	private MazeBuilderEller builderEller;
	private int width = 4;
	private int height = 4;
	
	@Before
	public void setup() {
		this.builderEller = new MazeBuilderEller();
		this.builderEller.width = this.width;
		this.builderEller.height = this.height;
		this.builderEller.floorplan = new Floorplan(4,4);
		//this.builderEller.populateMazeArray(builderEller.width, builderEller.height);
	}
	
	@Test
	/**
	 * Test case: see if MazeBuilderEller can generate a maze object and with specified width and height
	 * <p>
	 * Method Under Test: populateMazeArray()
	 * <p>
	 * Correct Behavior: a maze is generated with the correct width and height 
	 */
	public final void testMazeBuilderEller() {
		this.setup();
		assertNotNull(builderEller);
		assertEquals(builderEller.height, width);
		assertEquals(builderEller.width, height);
	}
	
	@Test
	/**
	 * Test case: see if the generated maze has the desired initial state
	 * <p>
	 * Method Under Test: populateMazeArray()
	 * <p>
	 * Correct Behavior: the numbers contained in each cell equals increase from 0 to width*height - 1
	 */
	public final void testMazeBuilderEllerInitializeMazeArray() {
		this.setup();
		this.builderEller.populateMazeArray(builderEller.width, builderEller.height);
		int count = 0;	
		for (int i = 0; i < builderEller.width; i++) {
			for (int j = 0; j < builderEller.height;j++) {
				assertEquals(builderEller.mazeArray[i][j], count);
				count++;
			}
		}
	}
	
	@Test
	/**
	 * Test case: see if there is a wall between two cells in a set 
	 * <p>
	 * Method Under Test: connectAdjacentCells()
	 * <p>
	 * Correct Behavior: no wall between two cells in a set 
	 */
	public final void testMazeBuilderConnectFirstRowMazeArray() {
		this.setup();
		this.builderEller.populateMazeArray(builderEller.width, builderEller.height);
		//connect all the horizontal cells in the first row
		builderEller.connectAdjacentCells(0);
		
		//first, if the mazearray[i][j] = mazearray[i][j+1], there is no wall inbetween
		for (int i = 0; i < builderEller.height-1; i++) {
			if (this.builderEller.mazeArray[0][i] == this.builderEller.mazeArray[0][i+1]) {
				assertEquals(this.builderEller.floorplan.hasWall(0, i, CardinalDirection.South), false);
			}
		}
	}
	
	@Test
	/**
	 * Test case: see if the cells stored in MazeArray have correct index of its set
	 * <p>
	 * Method Under Test: connectAdjacentCells()
	 * <p>
	 * Correct Behavior: the number stored in a cell should equal to the index of the set which contains it
	 */
	public final void testMazeBuilderConnectFirstRowConnectedSet() {
		this.setup();
		this.builderEller.populateMazeArray(builderEller.width, builderEller.height);
		//connect all the horizontal cells in the first row
		builderEller.connectAdjacentCells(0);
		//test the arraylist and see if it is correctly updated. 
		//it is correct if the posiiton in the mazearray is updated to the index value of 
		//the set in the total arraylist
		for (ArrayList<int[]> innerList: this.builderEller.connectedSets) {
			for (int[] position:innerList) {
				assertEquals(this.builderEller.mazeArray[position[0]][position[1]], this.builderEller.connectedSets.indexOf(innerList));
			}
		}
	}
	
	@Test
	/**
	 * Test case: see if there is at least one cell from each set connect to the cell in the next row
	 * <p>
	 * Method Under Test: connectVerticalCells()
	 * <p>
	 * Correct Behavior: total number of sets need to <= the number of deleted walls 
	 */
	public final void testMazeconnectVerticalRows() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		this.builderEller.connectAdjacentCells(0);
		this.builderEller.connectVerticalCells(0);
		
		//test that there are at least one wall deleted from each connected set. 
		int count = 0;
		for (int i = 0; i < this.builderEller.width; i++) {
			if (!this.builderEller.floorplan.hasWall(i, 0, CardinalDirection.East)){
				count++;
			}
		}
		assertEquals(true, this.builderEller.connectedSets.size()<= count);
	}
	
	
	@Test
	/**
	 * Test case:
	 * <p>
	 * Method Under Test:
	 * <p>
	 * Correct Behavior:
	 */
	public final void testMazeConnectedVerticalRowsConnectedSet() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		this.builderEller.connectAdjacentCells(0);
		this.builderEller.connectVerticalCells(0);
		//test that for each connected set, there are at least two coordinates that have the
		//same y coordinates and different x coordinate. 
		for (ArrayList<int[]> innerList: this.builderEller.connectedSets) {
			boolean result = false;
			for (int[] position:innerList) {
				for (int[] otherposition:innerList) {
					if (otherposition[0]== position[0]+1 && otherposition[1] == position[1]) {
						result = true;
					}
				}
			}
			assertEquals(result, true);
		}
	}
	

	@Test
	/**
	 * Test case: see if when both of the cells are not in a set, whether a new set can be created and add to connectedSets
	 * and if two cells have the corret index number of the set
	 * <p>
	 * Method Under Test: updateConnectedSetsHorizontal()
	 * <p>
	 * Correct Behavior: a new set is created and contain both cells and cells have correct index number of the set
	 */
	public final void testUpdateConnectedSetsHoriontal() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		int [] arr1 = new int[]{0,0};
		int [] arr2 = new int[]{0,1};
		this.builderEller.updateConnectedSetsHorizontal(arr1, arr2);
		assertEquals(this.builderEller.connectedSets.size(), 1);
		assertEquals(this.builderEller.connectedSets.get(0).get(0), arr1);
		assertEquals(this.builderEller.connectedSets.get(0).get(1), arr2);
		assertEquals(this.builderEller.mazeArray[0][0], this.builderEller.mazeArray[0][1]);
		assertEquals(this.builderEller.mazeArray[0][0], 0);
		assertEquals(this.builderEller.mazeArray[0][1], 0);
	}
	
	@Test
	/**
	 * Test case: see if new sets can be created in a row
	 * <p>
	 * Method Under Test: CreateNewArrayListAndAddToConnectedSets()
	 * <p>
	 * Correct Behavior: create new sets when no walls are deleted and the cell is not contained in any set
	 */
	public final void testCreateNewArrayListAndAddToConnectedSetsWhenEmpty() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		int [] arr1 = new int[]{0,0};
		int [] arr2 = new int[]{0,1};
		this.builderEller.createNewArrayListAndAddToConnectedSets(arr1);
		this.builderEller.createNewArrayListAndAddToConnectedSets(arr2);
		assertEquals(this.builderEller.connectedSets.size(), 2);
		assertEquals(this.builderEller.connectedSets.get(0).get(0), arr1);
		assertEquals(this.builderEller.connectedSets.get(1).get(0), arr2);
	}
	
	@Test
	/**
	 * Test case:
	 * <p>
	 * Method Under Test:
	 * <p>
	 * Correct Behavior:
	 */
	public final void testCreateNewArrayListAndAddToConnectedSetsWhenNotEmpty() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		this.builderEller.connectAdjacentCells(0);
		int size = this.builderEller.connectedSets.size();
		int [] arr1 = new int[]{1,0};
		int [] arr2 = new int[]{1,1};
		this.builderEller.createNewArrayListAndAddToConnectedSets(arr1);
		this.builderEller.createNewArrayListAndAddToConnectedSets(arr2);
		assertEquals(this.builderEller.connectedSets.size(), size+2);
		
		//add an already existing element into this array
		int [] arr3 = new int[]{0,0};
		this.builderEller.createNewArrayListAndAddToConnectedSets(arr3);
		assertEquals(this.builderEller.connectedSets.size(), size+2);
	}
	
	@Test
	/**
	 * Test case: see if a set can be found by providing a specific cell
	 * <p>
	 * Method Under Test: getArrayListFromConnectedSets()
	 * <p>
	 * Correct Behavior: a set is found by providing coordinate of a cell and null is returned when the cell does not belong to any set
	 */
	public final void testGetArrayListFromConnectedSets() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		int [] arr1 = new int[]{0,0};
		assertNull(this.builderEller.getArrayListFromConnectedSets(arr1));
		
		this.builderEller.connectAdjacentCells(0);
		assertNotNull(this.builderEller.getArrayListFromConnectedSets(arr1));
	}
	
	@Test
	/**
	 * Test case: see if numbers in cells can be changed to correct index number of the set after two adjacent cells are mergerd
	 * <p>
	 * Method Under Test: updateMazeArrayEntries()
	 * <p>
	 * Correct Behavior: both of the cells contain the specified index
	 */
	public final void testUpdateMazeArrayEntries() {
		this.setup();
		this.builderEller.generatePathways();
		int [] arr1 = new int[]{1,0};
		int [] arr2 = new int[]{1,1};
		this.builderEller.updateMazeArrayEntries(arr1, arr2, 99);
		assertEquals(this.builderEller.mazeArray[arr1[0]][arr1[1]], 99);
		assertEquals(this.builderEller.mazeArray[arr2[0]][arr2[1]], 99);
	}
	
	@Test
	/**
	 * Test case: see if numbers in cells can be changed to correct index number of the set after two vertical cells are mergerd
	 * <p>
	 * Method Under Test: UpdateConnectedSetsVertical()
	 * <p>
	 * Correct Behavior: both of the cells contain the specified index
	 */
	public final void testUpdateConnectedSetsVertical() {
		this.setup();
		this.builderEller.populateMazeArray(width, height);
		this.builderEller.connectAdjacentCells(0);
		int [] arr1 = new int[]{0,0};
		int [] arr2 = new int[]{1,0};
		this.builderEller.updateConnectedSetVertical(arr1, arr2, 0);
		assertEquals(this.builderEller.mazeArray[0][0], this.builderEller.mazeArray[1][0]);
	}
	
	@Test
	/**
	 * Test case: see if cells can be placed into correct sets after merging both adjacent and vertical cells
	 * <p>
	 * Method Under Test: checkIfCoordinateInConnectedSets()
	 * <p>
	 * Correct Behavior: before merging, cells are not contained in any sets; after merging, cells are contained in a set
	 */
	public final void testCheckIfCoordinateInConnectedSets() {
		this.setup();
		int [] arr1 = new int[]{0,0};
		int [] arr2 = new int[]{1,0};
		assertEquals(this.builderEller.checkIfCoordinateInConnectedSets(arr1), -1);
		assertEquals(this.builderEller.checkIfCoordinateInConnectedSets(arr2), -1);
		
		this.builderEller.populateMazeArray(width, height);
		this.builderEller.connectAdjacentCells(0);
		assertNotEquals(this.builderEller.checkIfCoordinateInConnectedSets(arr1), -1);
		assertEquals(this.builderEller.checkIfCoordinateInConnectedSets(arr2), -1);
		
		this.builderEller.connectVerticalCells(0);
		this.builderEller.connectAdjacentCells(1);
		assertNotEquals(this.builderEller.checkIfCoordinateInConnectedSets(arr1), -1);
		assertNotEquals(this.builderEller.checkIfCoordinateInConnectedSets(arr2), -1);
	}
	
	
	@Test
	/**
	 * Test case: see if all walls in the last row can be deleted
	 * <p>
	 * Method Under Test: populateLastRow()
	 * <p>
	 * Correct Behavior: no internal wall exist in the last row
	 */
	public final void testMazeLastRowMustBeEmpty() {	
		this.setup();
		this.builderEller.generatePathways();
		for (int j = 0; j < builderEller.height - 1; j++) {
			assertEquals(this.builderEller.floorplan.hasWall(builderEller.width-1, j, CardinalDirection.South), false);
		}
	}
	
}