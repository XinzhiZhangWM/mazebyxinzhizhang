package generation;

import gui.RobotDriver;

/**
 * This OrderStub class was created in order to temporarily implement the Order interface
 * for the ease 
 * @author vivianzhu
 *
 */

public class OrderStub implements Order{
  // need fields for skill level, builder, perfect, maze, percentage
	
	private int skillLevel;
	private Builder builder;
	private boolean isPerfect;
	private int percentage;
	private Maze mazeConfig;
	private RobotDriver robotDriver;
	
	public OrderStub(int skillLevel, Builder builder, boolean isPerfect) {
		this.skillLevel = skillLevel;
		this.builder = builder;
		this.isPerfect = isPerfect;
	}
	
	public OrderStub() {
		this.builder = null;
		this.mazeConfig = null;
	}
	
	@Override
	public int getSkillLevel() {
		// TODO Auto-generated method stub
		return this.skillLevel;
	}
	
	@Override
	public Builder getBuilder() {
		// TODO Auto-generated method stub
		return this.builder;
	}
	
	@Override
	public boolean isPerfect() {
		// TODO Auto-generated method stub
		return this.isPerfect;
	}
	
	public void setSkillLevel(int skillLevel) {
		this.skillLevel = skillLevel;
	}
	
	public void setBuilder(Builder builder) {
		this.builder = builder;
	}
	
	public void setperfect(boolean isPerfect) {
		this.isPerfect = isPerfect;
	}
	

	@Override
	public void deliver(Maze mazeConfig) {
		// TODO Auto-generated method stub
		this.mazeConfig = mazeConfig;
	}

	public Maze getMazeConfig() {
		return this.mazeConfig;
	}
	
	@Override
	public void updateProgress(int percentage) {
		// TODO Auto-generated method stub
		this.percentage = percentage;
	}
	
	public int getPercentage() {
		return this.percentage;
	}
//	@Override
//	public RobotDriver getRobotDriver() {
//		return this.robotDriver;
//	}
}
