package gui;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Distance;
import generation.Maze;
import generation.MazeFactory;
import generation.Order;
import generation.OrderStub;
import gui.Robot.Direction;

/**
 * This is test file to test implementation of the WallFollower algorithm.
 * The test cases are to test whether the driver has a correct configurations in the initial states.
 * It also tests whether the driver can successfully reach the exit when all sensors are operations,
 * and when some sensors are failed.
 * @author Lulu Zhang
 *
 */
class WallFollowerTest {

	private MazeFactory mazefactory;
	private Maze maze;
	private Distance distance;
	private Controller controller;
	private BasicRobot robot;
	private int width;
	private int height;
	private WallFollower driver;
	private Thread leftThread;
	private Thread forwardThread;
	private MazeApplication mazeApp;
	private Controller controller1;
	private BasicRobot robot1;
	private WallFollower driver1;
	private MazeFactory mazefactory1;
	private Maze maze1;
	private int width1;
	private int height1;
	private Distance distance1;
	private MazeApplication mazeApp1;
	
	/**
	 * This is a method to create maze object, robot, and also driver object.
	 * Maze are set to be perfect to test the Wall Follower algorithm. 
	 */
	@Before
	public void setUp() {
		OrderStub order = new OrderStub(1, Order.Builder.Eller, false);
		this.mazefactory = new MazeFactory(true);
		this.mazefactory.order(order);
		this.mazefactory.waitTillDelivered();
		this.maze = order.getMazeConfig();
		this.width = this.maze.getWidth();
		this.height = this.maze.getHeight();
		this.distance = this.maze.getMazedists();
		this.mazeApp = new MazeApplication("Eller");
		//this.controller = this.mazeApp.createController("Eller");
		this.controller = this.mazeApp.getController();
		this.controller.setBuilder(Order.Builder.Eller);
		this.controller.setPerfect(true);
		this.controller.setSkillLevel(1);
		this.controller.switchFromGeneratingToPlaying(this.maze);
		this.robot = new BasicRobot(this.controller);
		this.driver = new WallFollower(this.robot, this.distance, this.width, this.height);
		this.controller.setRobotAndDriver(this.robot, this.driver);
		
//		OrderStub order1 = new OrderStub(5, Order.Builder.Eller, false);
//		this.mazefactory1 = new MazeFactory(true);
//		this.mazefactory1.order(order1);
//		this.mazefactory1.waitTillDelivered();
//		this.maze1 = order1.getMazeConfig();
//		this.width1 = this.maze1.getWidth();
//		this.height1 = this.maze1.getHeight();
//		this.distance1 = this.maze1.getMazedists();
//		this.mazeApp1 = new MazeApplication("Eller");
//		this.controller1 = this.mazeApp1.getController();
//		this.controller1.setBuilder(Order.Builder.Eller);
//		this.controller1.setPerfect(true);
//		this.controller1.setSkillLevel(2);
//		this.controller1.switchFromGeneratingToPlaying(this.maze1);
//		this.robot1 = new BasicRobot(this.controller1);
//		this.robot1.setBatteryLevel(Integer.MAX_VALUE);
//		this.driver1 = new WallFollower(this.robot1, this.distance1, this.width1, this.height1);
//		this.controller1.setRobotAndDriver(this.robot1, this.driver1);
	}
	
	/**
	 * Test case: See if a robot in the initial state has correct configurations
	 * <p>
	 * Method Under Test: init(), BasicRobot()
	 * <p>
	 * Correct Behavior: Battery level is 3000, has all sensors, distance moved is 0, hasStopped is false
	 */
	@Test
	public void testInitialConfiguration() {
		this.setUp();
		Direction direction = Robot.Direction.LEFT;
		assertTrue(robot.hasOperationalSensor(direction));
		direction = Robot.Direction.RIGHT;
		assertTrue(robot.hasOperationalSensor(direction));
		direction = Robot.Direction.FORWARD;
		assertTrue(robot.hasOperationalSensor(direction));
		direction = Robot.Direction.BACKWARD;
		assertTrue(robot.hasOperationalSensor(direction));
		assertEquals(robot.getBatteryLevel(), 3000);
		assertEquals(robot.getOdometerReading(), 0);
		assertTrue(robot.hasRoomSensor());
		//exit sensor didn't test
		assertFalse(robot.hasStopped());
	}
	/**
	 * Test case: See if the energy consumption is correct in initial state
	 * <p>
	 * Method Under Test: getEnergyConsumption()
	 * <p>
	 * Correct Behavior: Initial Energy Consumption is 0
	 */
	@Test
	public void testInitialEnergyConsumption() {
		this.setUp();
		float batteryLevel = this.driver.getEnergyConsumption();
		assertEquals(0, batteryLevel);
	}
	/**
	 * Test case: See if the returned path length is correct in initial state
	 * <p>
	 * Method Under Test: getPathLength()
	 * <p>
	 * Correct Behavior: Initial path length is 0
	 */
	@Test
	public void testInitialPathLength() {
		this.setUp();
		int pathLength = this.driver.getPathLength();
		assertEquals(0, pathLength);
	}
	/**
	 * Test case: See if Wizard driver can reach exit when no sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitNoSensorFailed() {
		this.setUp();
		try {
			assertTrue(this.driver.drive2Exit());
			//assertTrue(this.driver1.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
//		assertNotEquals(0, this.driver1.getEnergyConsumption());
//		assertNotEquals(0, this.driver1.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when left sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitLeftSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.LEFT);
			assertTrue(this.driver.drive2Exit());
//			this.robot1.triggerSensorFailure(Direction.LEFT);
//			System.out.println("------test result------");
//			boolean result = this.driver1.drive2Exit();
//			System.out.println(result);
//			assertTrue(this.driver1.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
//		assertNotEquals(0, this.driver1.getEnergyConsumption());
//		assertNotEquals(0, this.driver1.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when right sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitRightSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.RIGHT);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when forward sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitForwardSensorFailed() {
		this.setUp(); 
		try {
			this.robot.triggerSensorFailure(Direction.FORWARD);
			assertTrue(this.driver.drive2Exit());
			//this.robot1.triggerSensorFailure(Direction.FORWARD);
			//assertTrue(this.driver1.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
//		assertNotEquals(0, this.driver1.getEnergyConsumption());
//		assertNotEquals(0, this.driver1.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when backward sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitBackwardSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.BACKWARD);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when Forward And Left sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard cannot reach the exit as it takes too much energy when the critical two sensors are failed
	 */
	@Test
	public void testDriver2ExitForwardAndLeftSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.FORWARD);
			this.robot.triggerSensorFailure(Direction.LEFT);
			assertFalse(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when right and left sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitRightAndLeftSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.RIGHT);
			this.robot.triggerSensorFailure(Direction.LEFT);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when backward and left sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitBackwardAndLeftSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.BACKWARD);
			this.robot.triggerSensorFailure(Direction.LEFT);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when forward and right sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitForwardAndRightSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.FORWARD);
			this.robot.triggerSensorFailure(Direction.RIGHT);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when forward and backward sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitForwardAndBackwardSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.FORWARD);
			this.robot.triggerSensorFailure(Direction.BACKWARD);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when right and backward sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitRightAndBackwardSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.RIGHT);
			this.robot.triggerSensorFailure(Direction.BACKWARD);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when forward, left, and right sensors are failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitForwardLeftRightSensorFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.FORWARD);
			this.robot.triggerSensorFailure(Direction.LEFT);
			this.robot.triggerSensorFailure(Direction.RIGHT);
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when right and backward sensor is failed
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitAllSensorsFailed() {
		this.setUp();
		try {
			this.robot.triggerSensorFailure(Direction.RIGHT);
			this.robot.triggerSensorFailure(Direction.BACKWARD);
			this.robot.triggerSensorFailure(Direction.FORWARD);
			this.robot.triggerSensorFailure(Direction.LEFT);
			assertFalse(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when left sensor thread is triggered
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitLeftSensorFailedUseLeftThread() {
		this.setUp();
		try {
			leftThread = new Thread(new RobotSensor(this.robot, this.driver, Direction.LEFT));
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}
	/**
	 * Test case: See if Wizard driver can reach exit when forward sensor thread is triggered
	 * <p>
	 * Method Under Test: drive2Exit()
	 * <p>
	 * Correct Behavior: Wizard should reach the exit as the maze is not very complicated and this algorithm is the most efficient one
	 */
	@Test
	public void testDriver2ExitForwardSensorFailedUseForwardThread() {
		this.setUp();
		try {
			forwardThread = new Thread(new RobotSensor(this.robot, this.driver, Direction.LEFT));
			assertTrue(this.driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotEquals(0, this.driver.getEnergyConsumption());
		assertNotEquals(0, this.driver.getPathLength());
	}

}
