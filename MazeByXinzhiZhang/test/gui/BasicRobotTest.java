package gui;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.CardinalDirection;
import generation.Distance;
import generation.Maze;
import generation.MazeFactory;
import generation.Order;
import generation.OrderStub;
import gui.Robot.Direction;

/**
 * This is a test file to test the implementation in BasicRobot.java.
 * The test cases test whether robot has correction functionalities to detect distance to obstacle, to rotate, move, or jump, etc.
 * @author Lulu Zhang
 *
 */
class BasicRobotTest {
	
	private Robot robot;
	private Controller controller;
	private Maze maze;
	private MazeFactory mazefactory;
	private Distance distance;
	
	@Before
	public void setUp() {
		OrderStub order = new OrderStub(9, Order.Builder.Eller, false);
		this.mazefactory = new MazeFactory(true);
		this.mazefactory.order(order);
		this.mazefactory.waitTillDelivered();
		this.maze = order.getMazeConfig();
		this.distance = this.maze.getMazedists();
		
		this.controller = new Controller();
		this.controller.setBuilder(Order.Builder.Eller);
		this.controller.setPerfect(false);
		this.controller.switchFromGeneratingToPlaying(this.maze);
		this.robot = new BasicRobot(this.controller);
		this.controller.setRobotAndDriver(this.robot, null);
	}
	
	/**
	 * Test case: See if a robot in the initial state has correct configurations
	 * <p>
	 * Method Under Test: init(), BasicRobot()
	 * <p>
	 * Correct Behavior: Battery level is 3000, has all sensors, distance moved is 0, hasStopped is false
	 */
	@Test
	void testInitialConfiguration() {
		this.setUp();
		Direction direction = Robot.Direction.LEFT;
		assertTrue(robot.hasOperationalSensor(direction));
		direction = Robot.Direction.RIGHT;
		assertTrue(robot.hasOperationalSensor(direction));
		direction = Robot.Direction.FORWARD;
		assertTrue(robot.hasOperationalSensor(direction));
		direction = Robot.Direction.BACKWARD;
		assertTrue(robot.hasOperationalSensor(direction));
		assertEquals(robot.getBatteryLevel(), 3000);
		assertEquals(robot.getOdometerReading(), 0);
		assertTrue(robot.hasRoomSensor());
		//exit sensor didn't test
		assertFalse(robot.hasStopped());
	}
	
	/**
	 *Test case: See if can get current position of the robot
	 * <p>
	 * Method Under Test: getCurrentPosition()
	 * <p>
	 * Correct Behavior: Position returned by robot should be the same as the position returned by controller
	 */
	@Test
	void testCurrentPosition() {
		this.setUp();
		int[] cpController = this.controller.getCurrentPosition();
		int[] cpRobot = null;
		try {
			cpRobot = robot.getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(cpController[0], cpRobot[0]);
		assertEquals(cpController[1], cpRobot[1]);
	}
	/**
	 *Test case: See if can throw an exception when the width is invalid
	 * <p>
	 * Method Under Test: getCurrentPosition()
	 * <p>
	 * Correct Behavior: exception is thrown
	 */
	@Test
	void testCurrentPositionInvalidWidth() {
		this.setUp();
		int width = this.maze.getWidth();
		int height = this.maze.getHeight();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(width + 1, height);
		try {
			int[] cpRobot = robot.getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 *Test case: See if can throw an exception when the height is invalid
	 * <p>
	 * Method Under Test: getCurrentPosition()
	 * <p>
	 * Correct Behavior: exception is thrown
	 */
	@Test
	void testCurrentPositionInvalidHeight() {
		this.setUp();
		int width = this.maze.getWidth();
		int height = this.maze.getHeight();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(width, height + 1);
		try {
			int [] cpRobot = robot.getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 *Test case: See if can get robot's cardinal direction
	 * <p>
	 * Method Under Test: getCurrentDiriont()
	 * <p>
	 * Correct Behavior: Robot's cardinal direction should be the same as controller's cardinal direction
	 */
	@Test
	void testCurrentDirection() {
		this.setUp();
		CardinalDirection dirController = this.controller.getCurrentDirection();
		CardinalDirection dirRobot = robot.getCurrentDirection();
		assertEquals(dirController, dirRobot);
	}
	
	//How to test this?
//	@Test
//	void testSetMaze() {
//		
//	}
	
	/**
	 *Test case: See of battery level can be reset to the passed integer parameter
	 * <p>
	 * Method Under Test: setBatteryLevel
	 * <p>
	 * Correct Behavior: Battery level after calling this method should equal to the passed integer parameter
	 */
	@Test
	void testSetBatteryLevel() {
		this.setUp();
		robot.setBatteryLevel(19);
		assertEquals(robot.getBatteryLevel(), 19);
	}
	/**
	 *Test case: See if odometer changes after moving the robot and odometer changes back to 0 after resetting it
	 * <p>
	 * Method Under Test: getOdometerReading, resetOdometer
	 * <p>
	 * Correct Behavior: Odometer is not equal to 0 after moving. Odometer equal to 0 after calling resetOdometer
	 */
	@Test
	void testGetOdometerReadingAndResetOdometer() {
		this.setUp();
		try {
			System.out.println("before");
			System.out.println(robot.getCurrentPosition()[0]);
			System.out.println(robot.getCurrentPosition()[1]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		robot.move(distance, true);
		
		try {
			System.out.println("after");
			System.out.println(robot.getCurrentPosition()[0]);
			System.out.println(robot.getCurrentPosition()[1]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(robot.getOdometerReading(), distance);
		robot.resetOdometer();
		assertEquals(robot.getOdometerReading(), 0);
	}
	/**
	 *Test case: See if can get correct amount of energy consumed if rotate 360 degrees. 
	 * <p>
	 * Method Under Test: getEnergyForFullRotation
	 * <p>
	 * Correct Behavior: Full rotation will consume 12 energy
	 */
	@Test
	void testgetEnergyForFullRotation() {
		this.setUp();
		assertEquals(robot.getEnergyForFullRotation(), 12);
		
	}
	/**
	 *Test case: See if can get correct amount of energy consumed if move 1 step.
	 * <p>
	 * Method Under Test: getEnergyForStepForward
	 * <p>
	 * Correct Behavior: Move one step will consume 5 energy.
	 */
	@Test
	void testGetEnergyForStepForward() {
		this.setUp();
		assertEquals(robot.getEnergyForStepForward(), 5);
	}
	
	/**
	 *Test case: See if robot can determine whether it's at the exit position or not
	 * <p>
	 * Method Under Test: isAtExit
	 * <p>
	 * Correct Behavior: It's correct if it returns false when not at exit and if it returns true when at exit
	 */	
	@Test
	void testIsAtExit() {
		this.setUp();
		
		//assume after setup, the robot is at the start position
		assertFalse(robot.isAtExit());

		//move several steps and not at exit
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		if (distance != Integer.MAX_VALUE) {
			robot.move(distance, true);
			assertFalse(robot.isAtExit());
		}
		
		//provide an input that is at exit, return true
		int[] exitPos = this.distance.getExitPosition();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(exitPos[0], exitPos[1]);
		assertTrue(robot.isAtExit());
	}
	
	/**
	 * Test case: See if robot can See Through The Exit Into Eternity when face exit
	 * <p>
	 * Method Under Test: canSeeThroughTheExitIntoEternity
	 * <p>
	 * Correct Behavior: Correct if it returns false when not facing the exit and if it returns true when it face the exit
	 */
	@Test
	void testCanSeeThroughTheExitIntoEternity() {
		this.setUp();
		CardinalDirection currentDir = robot.getCurrentDirection();
		//robot is at initial position
		int distance1 = robot.distanceToObstacle(Robot.Direction.FORWARD);
		int distance2 = robot.distanceToObstacle(Robot.Direction.BACKWARD);
		int distance3 = robot.distanceToObstacle(Robot.Direction.LEFT);
		int distance4 = robot.distanceToObstacle(Robot.Direction.RIGHT);
		int [] distances1 = {distance1, distance2, distance3, distance4};
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		for (int i = 0; i < 4; i++) {
			if (distances1[i] != Integer.MAX_VALUE) {
				assertFalse(robot.canSeeThroughTheExitIntoEternity(directions[i]));
			}
			else {
				assertTrue(robot.canSeeThroughTheExitIntoEternity(directions[i]));
			}
		}
		
		//set a robot at a position
		//test both when robot face the exit and not face the exit 
		int[] exitPos = this.distance.getExitPosition();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(exitPos[0], exitPos[1]);
		int distance5 = robot.distanceToObstacle(Robot.Direction.FORWARD);
		int distance6 = robot.distanceToObstacle(Robot.Direction.BACKWARD);
		int distance7 = robot.distanceToObstacle(Robot.Direction.LEFT);
		int distance8 = robot.distanceToObstacle(Robot.Direction.RIGHT);
		int [] distances2 = {distance5, distance6, distance7, distance8};
		for (int i = 0; i < 4; i++) {
			if (distances2[i] != Integer.MAX_VALUE) {
				assertFalse(robot.canSeeThroughTheExitIntoEternity(directions[i]));
			}
			else {
				assertTrue(robot.canSeeThroughTheExitIntoEternity(directions[i]));
			}
		}
	}
	/**
	 *Test case: See if disable a sensor, then call this method again, check whether exception can be thrown
	 * <p>
	 * Method Under Test: CanSeeThroughTheExitIntoEternityThrowException
	 * <p>
	 * Correct Behavior: An exception is thrown
	 */
	@Test
	void testCanSeeThroughTheExitIntoEternityThrowException() {
		this.setUp();
		robot.triggerSensorFailure(Robot.Direction.FORWARD);
		
		try {
			robot.canSeeThroughTheExitIntoEternity(Robot.Direction.FORWARD);
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 *Test case: See if a robot can correctly determine if it is inside a room
	 * <p>
	 * Method Under Test: IsInsideRoom()
	 * <p>
	 * Correct Behavior: If give a position which is inside a room, method should return true; give an invalid input, method should return false
	 */
	@Test
	void testIsInsideRoom() {
		this.setUp();
		try {
			int [] currentPos = robot.getCurrentPosition();
			boolean result = this.maze.getFloorplan().isInRoom(currentPos[0], currentPos[1]);
			if (result) {
				assertTrue(robot.isInsideRoom());
			}
			else {
				assertFalse(robot.isInsideRoom());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 *Test case: See if the robot has a room sensor
	 * <p>
	 * Method Under Test: HasRoomSensor
	 * <p>
	 * Correct Behavior: Correct if return true
	 */
	@Test
	void testHasRoomSensor() {
		this.setUp();
		assertEquals(robot.hasRoomSensor(), true);
	}
	/**
	 *Test case: See if robot can correctly change its status when battery level <= 0
	 * <p>
	 * Method Under Test: HasStopped
	 * <p>
	 * Correct Behavior: HasStopped should return false after setting up robot; HasStopped should return true after setting battery level = 0
	 */
	@Test
	void testHasStoppedBatteryLevel() {
		this.setUp();
		assertFalse(robot.hasStopped());
		robot.setBatteryLevel(0);
		assertTrue(robot.hasStopped());
	}
	/**
	 *Test case: See if robot can correctly change its status when face a wall and the game is not played manually
	 * <p>
	 * Method Under Test: HasStopped
	 * <p>
	 * Correct Behavior: HasStopped should return false after setting up robot; HasStopped should return true after move it to face a wall
	 */
	@Test
	void testHasStoppedFacingObstacle() {
		this.setUp();
		assertFalse(robot.hasStopped());
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		if (distance != Integer.MAX_VALUE) {
			robot.move(distance+1, false);
			assertTrue(robot.hasStopped());
		}
	}
	
	/**
	 *Test case: See if when robot face east and is not facing an exit, it can detects the distance in the forward direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	//1. set robot at a position facing exit, result = max_value
	//2. set the robot right before a wall, result = 0
	//3. set robot at exit position, but not face exit, result = 1(may change)
	//4. set robot 4 steps away a wall, result = 4
	//5. invalid direction-->throw exception
	@Test
	void testDistanceToObstacleForwardEast() {
		this.setUp();
		CardinalDirection currentDir = robot.getCurrentDirection();
		
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		System.out.println("Initial Position");
		System.out.println(distance);
		if (distance != Integer.MAX_VALUE) {
			robot.move(distance, true);
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face east and is not facing an exit, it can detects the distance in its left direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleLeftEast() {
		this.setUp();
		int distance = robot.distanceToObstacle(Robot.Direction.LEFT);
		
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.LEFT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face west and is not facing an exit, it can detects the distance in its right direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleRightWest() {
		this.setUp();
		robot.rotate(Robot.Turn.AROUND);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.RIGHT);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.RIGHT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face west and is not facing an exit, it can detects the distance in its left direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleLeftWest() {
		this.setUp();
		robot.rotate(Robot.Turn.AROUND);
		//CardinalDirection currentDir = robot.getCurrentDirection();
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.LEFT);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.LEFT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face west and is not facing an exit, it can detects the distance in its backward direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleBackwardWest() {
		this.setUp();
		robot.rotate(Robot.Turn.AROUND);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.BACKWARD);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.AROUND);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face North and is not facing an exit, it can detects the distance in its left direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleLeftNorth() {
		this.setUp();
		robot.rotate(Robot.Turn.RIGHT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.LEFT);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.LEFT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face North and is not facing an exit, it can detects the distance in its right direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleRightNorth() {
		this.setUp();
		robot.rotate(Robot.Turn.RIGHT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.RIGHT);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.RIGHT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face North and is not facing an exit, it can detects the distance in its backward direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleBackwardNorth() {
		this.setUp();
		robot.rotate(Robot.Turn.RIGHT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.BACKWARD);
		System.out.println("----------");
		System.out.println(distance);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.AROUND);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face Sorth and is not facing an exit, it can detects the distance in its left direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleLeftSorth() {
		this.setUp();
		robot.rotate(Robot.Turn.LEFT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.LEFT);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.LEFT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face Sorth and is not facing an exit, it can detects the distance in its right direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleRightSorth() {
		this.setUp();
		robot.rotate(Robot.Turn.LEFT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.RIGHT);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.RIGHT);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if when robot face Sorth and is not facing an exit, it can detects the distance in its backward direction correctly
	 * <p>
	 * Method Under Test: DistanceToObstacle
	 * <p>
	 * Correct Behavior: The distance detected should be consistent with the move() method. hasStopped should be false.
	 */
	@Test
	void testDistanceToObstacleBackwardSorth() {
		this.setUp();
		robot.rotate(Robot.Turn.LEFT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		
		int distance = robot.distanceToObstacle(Robot.Direction.BACKWARD);
		if (distance != Integer.MAX_VALUE) {
			robot.rotate(Robot.Turn.AROUND);
			robot.move(distance, true);
			CardinalDirection currentDir = robot.getCurrentDirection();
			int[] currentPos = this.controller.getCurrentPosition();
			assertTrue(this.maze.getFloorplan().hasWall(currentPos[0], currentPos[1], currentDir));
			assertFalse(robot.hasStopped());
		}
	}
	/**
	 *Test case: See if the game will be change to failure state when call DistanceToObstacleOutOfEnergy() and run out of energy
	 * <p>
	 * Method Under Test: DistanceToObstacleOutOfEnergy
	 * <p>
	 * Correct Behavior: Controller's current state should changed from StatePlaying to StateWinning
	 */
	@Test
	void testDistanceToObstacleOutOfEnergy() {
		this.setUp();
		robot.setBatteryLevel(1);
		robot.distanceToObstacle(Robot.Direction.BACKWARD);
		//assume after setup, robot is at start position, so robot run out of energy and not at exit, so game failed
		System.out.println(this.controller.currentState);
		assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
	}
	/**
	 *Test case: See if an exception is thrown when robot does not have a sensor in the specified direction
	 * <p>
	 * Method Under Test: DistanceToObstacleOutOfEnergy
	 * <p>
	 * Correct Behavior: An exception is thrown
	 */
	@Test
	void testDistanceToObstacleThrowException() {
		this.setUp();
		robot.triggerSensorFailure(Robot.Direction.LEFT);
		try {
			robot.distanceToObstacle(Robot.Direction.LEFT);
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/**
	 *Test case: See if sensors can be triggered failure successfully
	 * <p>
	 * Method Under Test: TriggerSensorFailure, HasOperationalSensor
	 * <p>
	 * Correct Behavior: hasOperationalSensor should return false after triggering the sensor failure of a specified direction
	 */
	@Test
	void testTriggerSensorFailureAndHasOperationalSensor() {
		this.setUp();
		robot.triggerSensorFailure(Robot.Direction.LEFT);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.LEFT));
		robot.triggerSensorFailure(Robot.Direction.RIGHT);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.RIGHT));
		robot.triggerSensorFailure(Robot.Direction.FORWARD);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.FORWARD));
		robot.triggerSensorFailure(Robot.Direction.BACKWARD);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.BACKWARD));
	}
	
	/**
	 *Test case: See if nonfunctional sensors can be repaired 
	 * <p>
	 * Method Under Test: RepairFailedSensor, triggerSensorFailure, hasOperationalSensor
	 * <p>
	 * Correct Behavior: Failed sensor should be functional again after calling RepairFailedSensor
	 */
	@Test
	void testRepairFailedSensor() {
		this.setUp();
		robot.triggerSensorFailure(Robot.Direction.LEFT);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.LEFT));
		assertTrue(robot.repairFailedSensor(Robot.Direction.LEFT));
		assertTrue(robot.hasOperationalSensor(Robot.Direction.LEFT));
		robot.triggerSensorFailure(Robot.Direction.RIGHT);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.RIGHT));
		assertTrue(robot.repairFailedSensor(Robot.Direction.RIGHT));
		assertTrue(robot.hasOperationalSensor(Robot.Direction.RIGHT));
		robot.triggerSensorFailure(Robot.Direction.FORWARD);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.FORWARD));
		assertTrue(robot.repairFailedSensor(Robot.Direction.FORWARD));
		assertTrue(robot.hasOperationalSensor(Robot.Direction.FORWARD));
		robot.triggerSensorFailure(Robot.Direction.BACKWARD);
		assertFalse(robot.hasOperationalSensor(Robot.Direction.BACKWARD));
		assertTrue(robot.repairFailedSensor(Robot.Direction.BACKWARD));
		assertTrue(robot.hasOperationalSensor(Robot.Direction.BACKWARD));
	}
	/**
	 *Test case: See if the robot's cardinal direction is changed correctly after rotating left
	 * <p>
	 * Method Under Test: rotate
	 * <p>
	 * Correct Behavior: Cardinal direction before rotation and after rotation should be consistent with the rotation direction.
	 *                   Battery level should decrease by 3
	 */                   
	@Test
	void testRotateLeft() {
		this.setUp();
		CardinalDirection currentDir = robot.getCurrentDirection();
		float batteryLevel = robot.getBatteryLevel();
		robot.rotate(Robot.Turn.LEFT);
		switch (currentDir) {
		case East:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.South);
			break;
		case West:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.North);
			break;
		case North:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.East);
			break;
		case South:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.West);
			break;
		}
		assertEquals(batteryLevel - 3, robot.getBatteryLevel());
	}
	/**
	 *Test case: See if the robot's cardinal direction is changed correctly after rotating right
	 * <p>
	 * Method Under Test: rotate
	 * <p>
	 * Correct Behavior: Cardinal direction before rotation and after rotation should be consistent with the rotation direction.
	 *                   Battery level should decrease by 3
	 */                   
	@Test
	void testRotateRight() {
		this.setUp();
		CardinalDirection currentDir = robot.getCurrentDirection();
		float batteryLevel = robot.getBatteryLevel();
		robot.rotate(Robot.Turn.RIGHT);
		switch (currentDir) {
		case East:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.North);
			break;
		case West:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.South);
			break;
		case North:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.West);
			break;
		case South:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.East);
			break;
		}
		assertEquals(batteryLevel - 3, robot.getBatteryLevel());
	}
	/**
	 *Test case: See if the robot will win the game after moving to exit position and facing the exit direction
	 * <p>
	 * Method Under Test: rotate
	 * <p>
	 * Correct Behavior: Game should win
	 */
	@Test
	void testRotateAtExit() {
		this.setUp();
		int[] exitPos = this.distance.getExitPosition();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(exitPos[0], exitPos[1]);
		robot.rotate(Robot.Turn.LEFT);
		Direction [] directions = {Robot.Direction.FORWARD, Robot.Direction.BACKWARD, Robot.Direction.LEFT, Robot.Direction.RIGHT};
		for (Direction dir : directions) {
			if (robot.canSeeThroughTheExitIntoEternity(dir) == true) {
				switch(dir) {
				case LEFT:
					robot.rotate(Robot.Turn.LEFT);
					assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
					break;
				case RIGHT:
					robot.rotate(Robot.Turn.RIGHT);
					assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
					break;
				case FORWARD:
					assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
					break;
				case BACKWARD:
					robot.rotate(Robot.Turn.AROUND);
					assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
					break;
				}	
			}
		}
	}
	/**
	 *Test case: See if the robot's cardinal direction is changed correctly after rotating around
	 * <p>
	 * Method Under Test: rotate
	 * <p>
	 * Correct Behavior: Cardinal direction before rotation and after rotation should be consistent with the rotation direction.
	 *                   Battery level should decrease by 6
	 */
	@Test
	void testRotateAround() {
		this.setUp();
		CardinalDirection currentDir = robot.getCurrentDirection();
		float batteryLevel = robot.getBatteryLevel();
		robot.rotate(Robot.Turn.AROUND);
		switch (currentDir) {
		case East:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.West);
			break;
		case West:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.East);
			break;
		case North:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.South);
			break;
		case South:
			currentDir = robot.getCurrentDirection();
			assertEquals(currentDir, CardinalDirection.North);
			break;
		}
		assertEquals(batteryLevel - 6, robot.getBatteryLevel());
	}
	/**
	 *Test case: See if robot will make the controller to change game state when running out of energy after rotating
	 * <p>
	 * Method Under Test: rotate()
	 * <p>
	 * Correct Behavior: Controller change the state from StatePlaying to StateWining
	 */
	@Test
	void testRotateOutOfEnergy() {
		this.setUp();
		robot.setBatteryLevel(1);
		robot.rotate(Robot.Turn.AROUND);
		//assume after setup, robot is at start position, so robot run out of energy and not at exit, so game failed
		System.out.println(this.controller.currentState);
		assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
	}
	/**
	 *Test case: See if the robot Robot can move to the correction position with the given distance
	 * <p>
	 * Method Under Test: move
	 * <p>
	 * Correct Behavior: Robot move to the correction position
	 */
	@Test
	void testMove() {
		this.setUp();
		float batteryLevel = robot.getBatteryLevel();
		int odoMeter = robot.getOdometerReading();
		int[] initialPos = this.controller.getCurrentPosition();
		CardinalDirection dir = robot.getCurrentDirection();
	
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		assertEquals(batteryLevel - 1, robot.getBatteryLevel());
		System.out.println("distance");
		System.out.print(distance);
		robot.move(distance, true);
		int[] currentPos = this.controller.getCurrentPosition();
		assertTrue(this.maze.hasWall(currentPos[0], currentPos[1], dir));
		assertEquals(batteryLevel - 1 - 5*distance, robot.getBatteryLevel());
		assertEquals(odoMeter + distance, robot.getOdometerReading());
		
		switch (dir) {
		case East:
			assertEquals(initialPos[0] + distance, currentPos[0]);
			assertEquals(initialPos[1], currentPos[1]);
			break;
		case West:
			assertEquals(initialPos[0] - distance, currentPos[0]);
			assertEquals(initialPos[1], currentPos[1]);
			break;
		case North:
			assertEquals(initialPos[0], currentPos[0]);
			assertEquals(initialPos[1] - distance, currentPos[1]);
			break;
		case South:
			assertEquals(initialPos[0], currentPos[0]);
			assertEquals(initialPos[1] + distance, currentPos[1]);
			break;
		}
		assertFalse(this.controller.getRobot().hasStopped()==true);
	}
	/**
	 *Test case: See if the robot Robot can move to the exit position and the game changes to winning state
	 * <p>
	 * Method Under Test: move
	 * <p>
	 * Correct Behavior: Robot move to the correction position, i.e., exit position, and the game changes to winning state
	 */
	@Test
	void testMoveToReachExit() {
		this.setUp();
		int[] exitPos = this.distance.getExitPosition();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(exitPos[0], exitPos[1]);
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		robot.move(distance, true);
		robot.rotate(Robot.Turn.AROUND);
		robot.move(distance, true);
		robot.rotate(Robot.Turn.RIGHT);
		assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
	}
	/**
	 * Test case: check if the robot can stop after hitting a wall while not manual
	 * <p>
	 * Method under test: move
	 * <p>
	 * It is correct if hasStopped becomes true
	 */
	@Test
	public void testMoveNotManual() {
		this.setUp();
		robot.move(2, false);
		assertTrue(this.controller.getRobot().hasStopped()==true);
	}
	/**
	 *Test case: See if robot will make the controller to change game state when running out of energy after moving
	 * <p>
	 * Method Under Test: move()
	 * <p>
	 * Correct Behavior: Controller change the state from StatePlaying to StateWining
	 */
	@Test
	void testMoveOutOfEnergy() {
		this.setUp();
		robot.setBatteryLevel(4);
		robot.rotate(Robot.Turn.RIGHT);
		robot.move(1, true);
		//assume after setup, robot is at start position, so robot run out of energy and not at exit, so game failed
		System.out.println(this.controller.currentState);
		assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
	}
	/**
	 *Test case: See if robot can jump to the correct position when face east
	 * <p>
	 * Method Under Test: jump
	 * <p>
	 * Correct Behavior: Robot jump to the correct position and battery level decreases by 50 after jumping
	 */
	@Test
	void testJumpRobotFaceEast() {
		this.setUp();
		float batteryLevel = robot.getBatteryLevel();
		int odoMeter = robot.getOdometerReading();
		CardinalDirection dir = robot.getCurrentDirection();
		
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		assertEquals(batteryLevel - 1, robot.getBatteryLevel());
		robot.move(distance, true);
		int[] initialPos = this.controller.getCurrentPosition();
		assertTrue(this.maze.hasWall(initialPos[0], initialPos[1], dir));
		assertEquals(batteryLevel - 1 - 5*distance, robot.getBatteryLevel());
		assertEquals(odoMeter + distance, robot.getOdometerReading());
		
		//know width and height, so know cells on four sides have exterior walls, use this to check
		try {
			robot.jump();
			int [] currentPos = this.controller.getCurrentPosition();
			assertEquals(batteryLevel - 1 - 5*distance - 50, robot.getBatteryLevel());
			assertEquals(odoMeter + distance + 1, robot.getOdometerReading());
			switch (dir) {
			case East:
				assertEquals(initialPos[0] + 1, currentPos[0]);
				assertEquals(initialPos[1], currentPos[1]);
				break;
			case West:
				assertEquals(initialPos[0] - 1, currentPos[0]);
				assertEquals(initialPos[1], currentPos[1]);
				break;
			case North:
				assertEquals(initialPos[0], currentPos[0]);
				assertEquals(initialPos[1] - 1, currentPos[1]);
				break;
			case South:
				assertEquals(initialPos[0], currentPos[0]);
				assertEquals(initialPos[1] + 1, currentPos[1]);
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 *Test case: See if robot can jump to the correct position when face north
	 * <p>
	 * Method Under Test: jump
	 * <p>
	 * Correct Behavior: Robot jump to the correct position and battery level decreases by 50 after jumping
	 */
	@Test
	void testJumpRobotFaceNorth() {
		this.setUp();
		float batteryLevel = robot.getBatteryLevel();
		int odoMeter = robot.getOdometerReading();
		robot.rotate(Robot.Turn.RIGHT);
		CardinalDirection dir = robot.getCurrentDirection();
		int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
		assertEquals(batteryLevel - 1 - 3, robot.getBatteryLevel());
		robot.move(distance, true);
		int[] initialPos = this.controller.getCurrentPosition();
		assertTrue(this.maze.hasWall(initialPos[0], initialPos[1], dir));
		assertEquals(batteryLevel - 1 - 3 - 5*distance, robot.getBatteryLevel());
		assertEquals(odoMeter + distance, robot.getOdometerReading());
		
		try {
			robot.jump();
			int [] currentPos = this.controller.getCurrentPosition();
			assertEquals(batteryLevel - 1 - 3 - 5*distance - 50, robot.getBatteryLevel());
			assertEquals(odoMeter + distance + 1, robot.getOdometerReading());
			switch (dir) {
			case East:
				assertEquals(initialPos[0] + 1, currentPos[0]);
				assertEquals(initialPos[1], currentPos[1]);
				break;
			case West:
				assertEquals(initialPos[0] - 1, currentPos[0]);
				assertEquals(initialPos[1], currentPos[1]);
				break;
			case North:
				assertEquals(initialPos[0], currentPos[0]);
				assertEquals(initialPos[1] - 1, currentPos[1]);
				break;
			case South:
				assertEquals(initialPos[0], currentPos[0]);
				assertEquals(initialPos[1] + 1, currentPos[1]);
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 *Test case: See if robot can throw an exception when jump outside of the maze
	 * <p>
	 * Method Under Test: jump
	 * <p>
	 * Correct Behavior: An exception is thrown
	 */
	@Test
	void testJumpThrowException() {
		this.setUp();
		((StatePlaying) this.controller.states[2]).setCurrentPosition(0, 0);
		robot.rotate(Robot.Turn.RIGHT);
		try {
			robot.jump();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 *Test case: See if robot will make the controller to change game state when running out of energy after jumping
	 * <p>
	 * Method Under Test: jump()
	 * <p>
	 * Correct Behavior: Controller change the state from StatePlaying to StateWining
	 */
	@Test
	void testJumpOutOfEnergy() {
		this.setUp();
		robot.setBatteryLevel(4);
		try {
			robot.jump();
			System.out.println("---------");
			assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//assume after setup, robot is at start position, so robot run out of energy and not at exit, so game failed
		System.out.println(this.controller.currentState);
	}
	/**
	 *Test case: See if robot will make the controller to change game state when running out of energy after rotating, moving, and jumping
	 * <p>
	 * Method Under Test: rotate(), move, jump
	 * <p>
	 * Correct Behavior: Controller change the state from StatePlaying to StateWining
	 */
	@Test
	void testMultipleOperationsOutOfEnergy() {
		this.setUp();
		robot.setBatteryLevel(130);
		try {
			robot.jump();
			robot.rotate(Robot.Turn.LEFT);
			int distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.move(distance, true);
			robot.rotate(Robot.Turn.RIGHT);
			distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.move(distance, true);
			System.out.println(robot.getBatteryLevel());
			robot.jump();
			robot.rotate(Robot.Turn.AROUND);
			distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.move(distance, true);
			System.out.println(robot.getBatteryLevel());
			robot.rotate(Robot.Turn.LEFT);
			distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.move(distance, true);
			System.out.println(robot.getBatteryLevel());
			robot.rotate(Robot.Turn.RIGHT);
			distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.move(distance, true);
			System.out.println(robot.getBatteryLevel());
			robot.rotate(Robot.Turn.RIGHT);
			distance = robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.move(distance, true);
			System.out.println(robot.getBatteryLevel());
			robot.distanceToObstacle(Robot.Direction.LEFT);
			robot.distanceToObstacle(Robot.Direction.FORWARD);
			robot.distanceToObstacle(Robot.Direction.RIGHT);
			robot.distanceToObstacle(Robot.Direction.BACKWARD);
			System.out.println(robot.getBatteryLevel());
			assertEquals(this.controller.getCurrentState(), this.controller.states[3]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
